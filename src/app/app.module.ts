import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FlagsComponent } from './flags/flags.component';
import { ReplacePipe } from './flags/helper/replace.pipe';

@NgModule({
  declarations: [
    AppComponent,
    FlagsComponent,
    ReplacePipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
