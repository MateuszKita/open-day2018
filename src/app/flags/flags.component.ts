import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-flags',
  templateUrl: './flags.component.html',
  styleUrls: ['./flags.component.css']
})
export class FlagsComponent implements OnInit {
  public countries: string[] = [
    'austria',
    'brazil',
    'czech-republic',
    'france',
    'germany',
    'greece',
    'ireland',
    'italy',
    'norway',
    'poland',
    'republic-of-china',
    'russia',
    'sweden',
    'switzerland',
    'turkey',
    'ukraine',
    'the-united-kingdom'
  ];

  constructor() {}

  ngOnInit() {}
}
